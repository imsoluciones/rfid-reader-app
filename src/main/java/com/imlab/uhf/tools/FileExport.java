package com.imlab.uhf.tools;

import android.os.Environment;
import android.util.Log;

import com.imlab.uhf.model.FileDto;
import com.imlab.uhf.model.TagInfoDto;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

public class FileExport {
    public static FileDto exportXls(File imAppsFolder, List<TagInfoDto> list) {
        try {
            String filename = "lectura" + getSufixTimestamp() + ".xls";
            // check if preprocess is needing
            File file = new File(imAppsFolder, filename);
            writeXLS(file, list);

            return new FileDto(true, file.getPath());
        } catch (Exception ex) {
            Log.i("FileExport exception", ex.getMessage());
            return new FileDto(false, "", ex.getMessage());
        }
    }

    public static String getSufixTimestamp() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd-HHmmss");
        Date curDate = new Date(System.currentTimeMillis());
        String dt = formatter.format(curDate);

        return dt;

    }

    public static void writeXLS(File destFile, List<TagInfoDto> tags) throws IOException, WriteException {
        // Log.i("FileExport", "Creating file " + path);
        WritableWorkbook wb = Workbook.createWorkbook(destFile);
        WritableSheet sheet = wb.createSheet("sheet1", 0);

        int col = 0;
        int row = 0;
        // add header
        Label hdrlabel = new Label(col, row++, "Product codes");
        sheet.addCell(hdrlabel);
        // add data
        for (TagInfoDto tagInfo : tags) {
            Label label = new Label(col, row++, tagInfo.getTagUii());
            sheet.addCell(label);

        }
        wb.write();
        wb.close();
    }

    public static FileDto exportTxt(File imAppsFolder, String content) {
        try {
            //Checking the availability state of the External Storage.
            String state = Environment.getExternalStorageState();
            if (!Environment.MEDIA_MOUNTED.equals(state)) {
                //If it isn't mounted - we can't write into it.
                throw new RuntimeException("External Storage not mounted");
            }
            // export content
            String filename = "lectdebug" + getSufixTimestamp() + ".txt";
            File file = new File(imAppsFolder, filename);

            try {
                FileOutputStream fos = new FileOutputStream(file);
                fos.write(content.getBytes());
                fos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            String retPath = file.getPath();

            return new FileDto(true, retPath);
        } catch (Exception ex) {
            Log.i("FileExport exception", ex.getMessage());
            return new FileDto(false, "", ex.getMessage());
        }
    }

}
