package com.imlab.uhf.fragment;

import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.imlab.uhf.R;
import com.imlab.uhf.activity.UHFMainActivity;
import com.imlab.uhf.model.AppConstants;
import com.imlab.uhf.model.FileDto;
import com.imlab.uhf.tools.FileExport;
import com.imlab.uhf.tools.StringUtils;
import com.imlab.uhf.tools.UIHelper;
import com.imlab.uhf.util.TagUtil;
import com.rscja.deviceapi.RFIDWithUHF;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class UHFReadTagFragment extends KeyDwonFragment {

    private Handler handler;
    private SimpleAdapter adapter;
    private TextView tvCount;
    //RadioGroup RgInventory;
    //RadioButton RbInventorySingle;
    //RadioButton RbInventoryLoop;
    private Button btInventory; // Start button
    private Button btClear;
    private Button btFilter;
    // private Button btImport;
    private Button btExport;
    private ListView LvTags;
    private PopupWindow popFilter;
    private boolean loopFlag = false;
    // demo 0.2 always cycle read
    private int inventoryFlag = 1;
    private List<Map<String, String>> tagList;
    // private LinearLayout llContinuous;
    private UHFMainActivity mContext;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i("MY", "UHFReadTagFragment.onCreateView");
        return inflater
                .inflate(R.layout.uhf_readtag_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        Log.i("MY", "UHFReadTagFragment.onActivityCreated");
        super.onActivityCreated(savedInstanceState);
        mContext = (UHFMainActivity) getActivity();
        tagList = new ArrayList<>();

        btInventory = getView().findViewById(R.id.btInventory);
        btClear = getView().findViewById(R.id.btClear);
        btFilter = getView().findViewById(R.id.btFilter);
        // btImport = getView().findViewById(R.id.btImport);
        btExport = getView().findViewById(R.id.btExport);

        tvCount = getView().findViewById(R.id.labTagCount);
        // RgInventory = getView().findViewById(R.id.RgInventory);
        String tr = "";
        // RbInventorySingle = getView().findViewById(R.id.RbInventorySingle);
        // RbInventoryLoop = getView().findViewById(R.id.RbInventoryLoop);

        LvTags = getView().findViewById(R.id.lvTags);

        // llContinuous = getView().findViewById(R.id.llContinuous);

        adapter = new SimpleAdapter(mContext, tagList, R.layout.listtag_items,
                new String[]{AppConstants.UHF_TAG_UII, AppConstants.UHF_TAG_LEN, AppConstants.UHF_TAG_COUNT, AppConstants.UHF_TAG_RSSI},
                new int[]{R.id.TvTagUii, R.id.TvTagLen, R.id.TvTagCount,
                        R.id.TvTagRssi});

        btInventory.setOnClickListener(new BtInventoryClickListener());
        btClear.setOnClickListener(new BtClearClickListener());
        // btImport.setOnClickListener(new BtImportClickListener());
        btExport.setOnClickListener(new BtExportClickListener());
        // RgInventory.setOnCheckedChangeListener(new RgInventoryCheckedListener());

        btFilter.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popFilter == null) {
                    View viewPop = LayoutInflater.from(mContext).inflate(R.layout.popwindow_filter, null);

                    popFilter = new PopupWindow(viewPop, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT, true);

                    popFilter.setTouchable(true);
                    popFilter.setOutsideTouchable(true);
                    popFilter.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                    popFilter.setBackgroundDrawable(new BitmapDrawable());

                    // final EditText etLen = viewPop.findViewById(R.id.etLen);
                    // final EditText etPtr = viewPop.findViewById(R.id.etPtr);
                    final EditText etData = viewPop.findViewById(R.id.etData);
                    final RadioButton rbEPC = viewPop.findViewById(R.id.rbEPC);
                    final RadioButton rbTID = viewPop.findViewById(R.id.rbTID);
                    final RadioButton rbUser = viewPop.findViewById(R.id.rbUser);
                    final Button btSet = viewPop.findViewById(R.id.btSet);


                    btSet.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            String filterBank = "UII";
                            if (rbEPC.isChecked()) {
                                filterBank = "UII";
                            } else if (rbTID.isChecked()) {
                                filterBank = "TID";
                            } else if (rbUser.isChecked()) {
                                filterBank = "USER";
                            }
                            /*
                            if (etLen.getText().toString() == null || etLen.getText().toString().isEmpty()) {
                                UIHelper.ToastMessage(mContext, "Data length cannot be empty");
                                return;
                            }
                            if (etPtr.getText().toString() == null || etPtr.getText().toString().isEmpty()) {
                                UIHelper.ToastMessage(mContext, "The start address cannot be empty");
                                return;
                            }
                            int ptr = StringUtils.toInt(etPtr.getText().toString(), 0);
                            int len = StringUtils.toInt(etLen.getText().toString(), 0);
                             */
                            int ptr = 32;
                            int len = 0;
                            String data = etData.getText().toString().trim();
                            if (len > 0) {
                                String rex = "[\\da-fA-F]*";
                                if (data == null || data.isEmpty() || !data.matches(rex)) {
                                    UIHelper.ToastMessage(mContext, "The filtered data must be hexadecimal data");
                                    // mContext.playSound(2);
                                    return;
                                }
                                // Setear filtro en reader
                                // TODO revisar!!!!
                                if (mContext.mReader.setFilter(RFIDWithUHF.BankEnum.valueOf(filterBank), ptr, len, data, false)) {
                                    UIHelper.ToastMessage(mContext, R.string.uhf_msg_set_filter_succ);
                                } else {
                                    UIHelper.ToastMessage(mContext, R.string.uhf_msg_set_filter_fail);
                                    // mContext.playSound(2);
                                }
                            } else {
                                //Disable filtering
                                String dataStr = "";
                                if (mContext.mReader.setFilter(RFIDWithUHF.BankEnum.valueOf("UII"), 0, 0, dataStr, false)
                                        && mContext.mReader.setFilter(RFIDWithUHF.BankEnum.valueOf("TID"), 0, 0, dataStr, false)
                                        && mContext.mReader.setFilter(RFIDWithUHF.BankEnum.valueOf("USER"), 0, 0, dataStr, false)) {
                                    UIHelper.ToastMessage(mContext, R.string.msg_disable_succ);
                                } else {
                                    UIHelper.ToastMessage(mContext, R.string.msg_disable_fail);
                                }
                            }


                        }
                    });
                    CheckBox cbFilter = viewPop.findViewById(R.id.cb_filter);
                    /*
                    rbEPC.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (rbEPC.isChecked()) {
                                etPtr.setText("32");
                            }
                        }
                    });
                    rbTID.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (rbTID.isChecked()) {
                                etPtr.setText("0");
                            }
                        }
                    });
                    rbUser.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (rbUser.isChecked()) {
                                etPtr.setText("0");
                            }
                        }
                    });
                    */

                    cbFilter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            if (isChecked) { //Enable filtering

                            } else { //Disable filtering

                            }
                            popFilter.dismiss();
                        }
                    });
                }
                if (popFilter.isShowing()) {
                    popFilter.dismiss();
                    popFilter = null;
                } else {
                    popFilter.showAsDropDown(view);
                }
            }
        });
        LvTags.setAdapter(adapter);
        clearData();
        Log.i("MY", "UHFReadTagFragment.EtCountOfTags=" + tvCount.getText());
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                String result = msg.obj + "";
                String[] strs = result.split("@");
                addEPCToList(strs[0], strs[1]);
                mContext.playSound(1);
            }
        };
    }

    @Override
    public void onPause() {
        Log.i("MY", "UHFReadTagFragment.onPause");
        super.onPause();

        stopInventory();
    }

    private void addEPCToList(String epc, String rssi) {
        if (!TextUtils.isEmpty(epc)) {
            int index = checkIfExist(epc);
            // mContext.getAppContext().uhfQueue.offer(epc + "\t 1");
            if (index == -1) {
                // no se encuentra => Agregar
                Map<String, String> map = new HashMap<>();
                map.put(AppConstants.UHF_TAG_UII, epc);
                map.put(AppConstants.UHF_TAG_COUNT, String.valueOf(1));
                map.put(AppConstants.UHF_TAG_RSSI, rssi);

                tagList.add(map);
                LvTags.setAdapter(adapter);
                tvCount.setText("" + adapter.getCount());
            } else {
                // se encuentra => No hacer nada
                // int tagcount = Integer.parseInt(tagList.get(index).get("tagCount"), 10) + 1;
                // tagList.get(index).put("tagCount", String.valueOf(tagcount));
            }
            adapter.notifyDataSetChanged();
        }
    }

    private void clearData() {
        tvCount.setText("0");

        tagList.clear();

        Log.i("MY", "tagList.size " + tagList.size());

        adapter.notifyDataSetChanged();
    }

    private void readTag() {
        if (btInventory.getText().equals(mContext.getString(R.string.btInventory))) {
            // Identification label
            switch (inventoryFlag) {
                case 0:// Single step
                {
                    String strUII = mContext.mReader.inventorySingleTag();
                    if (!TextUtils.isEmpty(strUII)) {
                        String strEPC = mContext.mReader.convertUiiToEPC(strUII);
                        addEPCToList(strEPC, "N/A");
                        tvCount.setText("" + adapter.getCount());
                    } else {
                        UIHelper.ToastMessage(mContext, R.string.uhf_msg_inventory_fail);
                        //	mContext.playSound(2);
                    }
                }
                break;
                case 1:// Single label loop  .startInventoryTag((byte) 0, (byte) 0))
                {
                    //  mContext.mReader.setEPCTIDMode(true);
                    if (mContext.mReader.startInventoryTag(0, 0)) {
                        btInventory.setText(mContext
                                .getString(R.string.title_stop_Inventory));
                        loopFlag = true;
                        setViewEnabled(false);
                        new TagThread().start();
                    } else {
                        mContext.mReader.stopInventory();
                        UIHelper.ToastMessage(mContext,
                                R.string.uhf_msg_inventory_open_fail);
                        // mContext.playSound(2);
                    }
                }
                break;
                default:
                    break;
            }
        } else {
            // Stop recognition
            stopInventory();
        }
    }

    private void setViewEnabled(boolean enabled) {
        // RbInventorySingle.setEnabled(enabled);
        // RbInventoryLoop.setEnabled(enabled);
        btFilter.setEnabled(enabled);
        btClear.setEnabled(enabled);
    }

    /**
     * Stop recognition
     */
    private void stopInventory() {
        if (loopFlag) {
            loopFlag = false;
            setViewEnabled(true);
            if (mContext.mReader.stopInventory()) {
                btInventory.setText(mContext.getString(R.string.btInventory));
            } else {
                UIHelper.ToastMessage(mContext,
                        R.string.uhf_msg_inventory_stop_fail);
            }
        }
    }

    /**
     * Determine if EPC is in the list
     *
     * @param strEPC index
     * @return
     */
    public int checkIfExist(String strEPC) {
        int existFlag = -1;
        if (StringUtils.isEmpty(strEPC)) {
            return existFlag;
        }
        String tempStr = "";
        for (int i = 0; i < tagList.size(); i++) {
            Map<String, String> temp;
            temp = tagList.get(i);
            tempStr = temp.get(AppConstants.UHF_TAG_UII);
            if (strEPC.equals(tempStr)) {
                existFlag = i;
                break;
            }
        }
        return existFlag;
    }

    @Override
    public void myOnKeyDwon() {
        readTag();
    }

    public class BtClearClickListener implements OnClickListener {

        @Override
        public void onClick(View v) {

            clearData();

        }
    }

    public class BtImportClickListener implements OnClickListener {
        @Override
        public void onClick(View v) {
            UIHelper.ToastMessage(mContext, "Proceso no implementado.");
            return;
            /*
            // TODO
            if (btInventory.getText().equals(
                    mContext.getString(R.string.btInventory))) {
                if (tagList.size() == 0) {

                    UIHelper.ToastMessage(mContext, "No data export");
                    return;
                }
                boolean re = FileExport.daochu("", tagList);
                if (re) {
                    UIHelper.ToastMessage(mContext, "Successfully exported");
                    tvCount.setText("0");
                    tagList.clear();
                    Log.i("MY", "tagList.size " + tagList.size());
                    adapter.notifyDataSetChanged();
                }
            } else {
                UIHelper.ToastMessage(mContext, "Please stop scanning before exporting");
            }
             */
        }
    }

    public class BtExportClickListener implements OnClickListener {
        @Override
        public void onClick(View v) {
            /*
            // sample txt
            FileDto fileDto = FileExport.exportTxt(TagUtil.getRandom(20), mContext.getExternalFilesDir("lecturas"));
            if (fileDto.isRe()) {
                UIHelper.ToastMessage(mContext, "Archivo generado: " + fileDto.getDestFilename());

                tvCount.setText("0");

                tagList.clear();

                // Log.i("MY", "tagList.size " + tagList.size());

                adapter.notifyDataSetChanged();
            } else {
                // something happened
                UIHelper.ToastMessage(mContext, "Error al generar archivo: " + fileDto.getErrMsg());
            }
            */
            // export xls
            if (btInventory.getText().equals(
                    mContext.getString(R.string.btInventory))) {
                if (tagList.size() == 0) {
                    UIHelper.ToastMessage(mContext, "No hay datos para exportar");
                    return;
                }
                FileDto fileDto = FileExport.exportXls(mContext.getExternalFilesDir("lecturas"), TagUtil.mapToDtos(tagList));
                if (fileDto.isRe()) {
                    UIHelper.ToastMessage(mContext, "Archivo generado: " + fileDto.getDestFilename());

                    tvCount.setText("0");

                    tagList.clear();

                    Log.i("MY", "tagList.size " + tagList.size());

                    adapter.notifyDataSetChanged();
                } else {
                    // something happened
                    UIHelper.ToastMessage(mContext, "Error al generar archivo: " + fileDto.getErrMsg());
                }
            } else {
                UIHelper.ToastMessage(mContext, "Debe parar de escanear para exportar los datos");
            }
        }
    }

    /*
    public class RgInventoryCheckedListener implements OnCheckedChangeListener {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            llContinuous.setVisibility(View.GONE);
            if (checkedId == RbInventorySingle.getId()) {
                // Single step recognition
                inventoryFlag = 0;
            } else if (checkedId == RbInventoryLoop.getId()) {
                // Single label cycle recognition
                inventoryFlag = 1;
                llContinuous.setVisibility(View.VISIBLE);
            }
        }
    }
     */

    public class BtInventoryClickListener implements OnClickListener {
        @Override
        public void onClick(View v) {
            readTag();
        }
    }

    class TagThread extends Thread {
        public void run() {
            String strTid;
            String strResult;
            String[] res = null;
            while (loopFlag) {
                res = mContext.mReader.readTagFromBuffer();
                if (res != null) {
                    int i = 0;
                    //String debContent = "";
                    //for (String ri : res) {
                    //    debContent += "RFIDx_" + i++ + ": " + ri;
                    //}
                    strTid = res[0];
                    if (strTid.length() != 0 && !strTid.equals("0000000" +
                            "000000000") && !strTid.equals("000000000000000000000000")) {
                        strResult = "TID:" + strTid + "\n";
                    } else {
                        strResult = "";
                    }
                    Log.i("data", "EPC:" + res[1] + "|" + strResult);
                    // FileExport.exportTxt(mContext.getExternalFilesDir("lecturas"), debContent);

                    Message msg = handler.obtainMessage();
                    msg.obj = strResult + "EPC:" + mContext.mReader.convertUiiToEPC(res[1]) + "@" + res[2];

                    handler.sendMessage(msg);
                }
            }
        }
    }

}
