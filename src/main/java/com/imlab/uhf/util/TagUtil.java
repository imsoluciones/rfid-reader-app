package com.imlab.uhf.util;

import android.widget.ArrayAdapter;

import com.imlab.uhf.model.AppConstants;
import com.imlab.uhf.model.TagInfoDto;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class TagUtil {
    public static List<TagInfoDto> mapToDtos(List<Map<String, String>> tags) {
        List<TagInfoDto> list = new ArrayList<>();
        for (Map<String, String> mt : tags) {
            list.add(mapToDto(mt));
        }

        return list;
    }

    public static TagInfoDto mapToDto(Map<String, String> mt) {
        TagInfoDto ret = new TagInfoDto();
        if (mt.containsKey(AppConstants.UHF_TAG_UII)) {
            ret.setTagUii(mt.get(AppConstants.UHF_TAG_UII));
        }
        if (mt.containsKey(AppConstants.UHF_TAG_COUNT)) {
            ret.setTagCount(mt.get(AppConstants.UHF_TAG_COUNT));
        }
        if (mt.containsKey(AppConstants.UHF_TAG_RSSI)) {
            ret.setTagRssi(mt.get(AppConstants.UHF_TAG_RSSI));
        }

        return ret;
    }

    public static List<TagInfoDto> getRandom(int qty) {
        List<TagInfoDto> ret = new ArrayList<>();
        for (int i = 0; i < qty; i++) {
            ret.add(generateRandom());
        }
        return ret;
    }

    public static TagInfoDto generateRandom() {
        TagInfoDto ret = new TagInfoDto();
        String zeros = "0000000000000000";
        Random rnd = new Random();
        String s = Integer.toString(rnd.nextInt(0X1000000), 16);
        s = zeros.substring(s.length()) + s;

        ret.setTagUii(s);
        ret.setTagCount(String.valueOf(1));
        ret.setTagRssi("00");

        return ret;
    }

}
