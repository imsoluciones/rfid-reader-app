package com.imlab.uhf.model;

import java.io.Serializable;

public class TagInfoDto implements Serializable {
    private String tagUii;
    private String tagCount;
    private String tagRssi;

    public TagInfoDto() {
    }

    public String getTagUii() {
        return tagUii;
    }

    public void setTagUii(String tagUii) {
        this.tagUii = tagUii;
    }

    public String getTagCount() {
        return tagCount;
    }

    public void setTagCount(String tagCount) {
        this.tagCount = tagCount;
    }

    public String getTagRssi() {
        return tagRssi;
    }

    public void setTagRssi(String tagRssi) {
        this.tagRssi = tagRssi;
    }
}
