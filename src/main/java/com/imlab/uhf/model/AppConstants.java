package com.imlab.uhf.model;

public interface AppConstants {
    String UHF_TAG_UII = "tagUii";
    String UHF_TAG_COUNT = "tagCount";
    String UHF_TAG_RSSI = "tagRssi";
    String UHF_TAG_LEN = "tagLen";
}
