package com.imlab.uhf.model;

import java.io.Serializable;

public class FileDto implements Serializable {
    private boolean re;
    private String destFilename;
    private String errMsg;

    public FileDto() {
    }

    public FileDto(boolean re, String destFilename) {
        this.re = re;
        this.destFilename = destFilename;
    }

    public FileDto(boolean re, String destFilename, String errMsg) {
        this.re = re;
        this.destFilename = destFilename;
        this.errMsg = errMsg;
    }

    public boolean isRe() {
        return re;
    }

    public void setRe(boolean re) {
        this.re = re;
    }

    public String getDestFilename() {
        return destFilename;
    }

    public void setDestFilename(String destFilename) {
        this.destFilename = destFilename;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }
}
